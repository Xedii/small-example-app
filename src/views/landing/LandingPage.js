import React, { useCallback, memo } from 'react';
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'


import { Banner } from './components/Banner';
import { Chart } from './components/Chart';
import { Select } from 'lib/components/Select';

import { useReducerLanding } from './Reducer';

import './landing.scss'

export const Landing = () => {
    const { state, actions } = useReducerLanding();

    const changeSelect = useCallback((field) => (data) => {
      actions.setFilter(field, data)
    }, [])
    
    return (
        <Container className="h-100 landing-page">
          <Banner />
          <Row>
            <Col className="side-bar" sm={3}>
              <h4 className="title" >Filter dimensions values</h4>
              {state.options.map(option => (
                <Select
                  key={option.name}
                  name={option.name}
                  options={option.options}
                  onChange={changeSelect(option.name)}
                  value={state.filterData[option.name]}
                  isMulti
                />
              ))}
            </Col>
            <Col sm={9}>
              <Chart 
                dataSeries={state.dataSeries}
                Xaxis='date'
                lineData={[
                  {
                    name: 'clicks',
                    yAxisId: "clicks"
                  },
                  {
                    name: 'impressions',
                    yAxisId: "impressions"
                  }
                ]}
                Yxis={[
                  {
                    name: 'clicks',
                    yAxisId: 'clicks',
                    type:'number',
                    description:'Clicks',
                    orientation:'left',
                  },
                  {
                    name: 'impressions',
                    yAxisId: "impressions",
                    type:"number",
                    description:"impressions",
                    orientation:"right",
                  }
                ]}
                legend
                tooltip
              />
            </Col>
          </Row>
        </Container>
    )
}

const propsAreEqual = (prevProps, nextProps) => {
  return prevProps.state.dataSeries === nextProps.state.dataSeries
    && prevProps.state.options === nextProps.state.options
    && prevProps.state.filterData === nextProps.state.filterData
    && prevProps.state.filterData === nextProps.state.filterData
}

export const LandingPage = memo(Landing, propsAreEqual);
