import React from 'react';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Container from 'react-bootstrap/Container';

export const Banner = () => {
  return (
    <Jumbotron fluid>
      <Container>
        <h1>Adverity Advertising Data ETL-V Challange </h1>
        <p className="info-text">Select zero to N Datasource</p>
        <p className="info-text">Select zero to N Campaigns</p>
      </Container>
    </Jumbotron>
  );
};
