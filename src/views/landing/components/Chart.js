import React, { memo } from 'react'
import PropTypes from 'prop-types';
import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, Legend, ResponsiveContainer } from 'recharts';

const ChartComponent = ({ dataSeries, Xaxis, Yxis, lineData, legend, tooltip }) => (
    <div style={{ width: '100%', height: '100%' }}>
        <ResponsiveContainer>
            <LineChart data={dataSeries}>
                <CartesianGrid stroke="#ccc" />
                {Xaxis && <XAxis dataKey={Xaxis} />}
                {lineData.map(line => (
                    <Line type="monotone" key={line.yAxisId} yAxisId={line.yAxisId} dataKey={line.name} activeDot={{ r: 8 }} />
                ))}
                {Yxis.map(xis => (
                    <YAxis 
                        key={`${xis.yAxisId}-${xis.name}`} 
                        orientation={xis.orientation}
                        name={xis.name}
                        type={xis.type}
                        yAxisId={xis.yAxisId}
                        width={100}
                        label={{ value: xis.description, position: xis.orientation }}
                    />
                ))}
                {tooltip && <Tooltip />}
                {legend && <Legend />}
            </LineChart>
        </ResponsiveContainer>
    </div>
)

ChartComponent.propTypes = {
    Xaxis: PropTypes.string,
    legend: PropTypes.bool,
    tooltip: PropTypes.bool,
    lineData: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        yAxisId: PropTypes.string.isRequired
      })).isRequired,
    dataSeries: PropTypes.array.isRequired
  };

const propsAreEqual = (prevProps, nextProps) => {
    return prevProps.dataSeries === nextProps.dataSeries;
  }

export const Chart = memo(ChartComponent, propsAreEqual);

