import _ from 'lodash';

export function processCsv(response = [], model) {
  if (!response || !response.length) return;
  const [header, ...parseToObject] = response;

  return _.chain(parseToObject)
    .map(el => _.zipObject(header, el))
    .map(model)
    .value();
}

export default processCsv;
