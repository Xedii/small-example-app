import React from 'react';
import PropTypes from 'prop-types';
import ReactSelect from 'react-select';

import './select.scss';

export const Select = ({ name, onChange, options, label, isMulti, value }) => (
  <div className="select-option">
    {name && <p>{name}</p>}
    <ReactSelect
      label={label}
      isMulti={isMulti}
      value={value}
      onChange={onChange}
      options={options}
    />
  </div>
);

Select.propTypes = {
  name: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
    })
  ).isRequired,
  label: PropTypes.string,
  isMulti: PropTypes.bool.isRequired,
  value: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string.isRequired,
      label: PropTypes.string.isRequired,
    })
  ),
};

export default Select;
